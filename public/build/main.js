webpackJsonp([0],{

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(nav, authService, alertCtrl, loadingCtrl) {
        this.nav = nav;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    LoginPage.prototype.signup = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        // require email and password
        if (!this.email || !this.password) {
            var alert_1 = this.alertCtrl.create({
                message: 'Please provide email and password',
                buttons: ['OK']
            });
            return alert_1.present();
        }
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.authService.login(this.email, this.password).then(function (authData) {
            loading.dismiss();
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }, function (error) {
            // in case of login error
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                message: error.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    // login with facebook
    LoginPage.prototype.loginWithFacebook = function () {
        var _this = this;
        this.authService.loginWithFacebook().subscribe(function (authData) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }, function (error) {
            // in case of login error
            var alert = _this.alertCtrl.create({
                message: error.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    // login with google
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        this.authService.loginWithGoogle().subscribe(function (authData) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }, function (error) {
            // in case of login error
            var alert = _this.alertCtrl.create({
                message: error.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content class=" auth-bg">\n\n  <div class="auth-content">\n\n    <div class="light-bg">\n\n      <!-- Logo -->\n      <div padding text-center>\n        <div class="logo primary-bg">\n          <ion-icon name="ios-car" color="light"></ion-icon>\n        </div>\n        <h2 ion-text color="dark">\n          Firetaxi\n        </h2>\n        <p ion-text color="dark">Firebase taxi booking app</p>\n      </div>\n\n      <!-- Login form -->\n\n      <ion-list class="list-form" padding>\n\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="email" placeholder="Email or username"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-input type="password" [(ngModel)]="password" placeholder="Password"></ion-input>\n        </ion-item>\n\n      </ion-list>\n\n      <div padding>\n        <button ion-button color="primary" block (click)="login()">\n          Login\n        </button>\n        <button ion-button block color="fb-color" (click)="loginWithFacebook()">\n          <span>Sign in with Facebook</span>\n        </button>\n        <button ion-button block color="gg-color" (click)="loginWithGoogle()">\n          <span ion-text color="danger">Sign in with Google</span>\n        </button>\n      </div>\n\n    </div>\n\n    <div padding=""></div>\n\n    <!-- Other links -->\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <span ion-text color="light">Forgot password?</span>\n        </ion-col>\n        <ion-col>\n          <span ion-text color="light">\n            New here?\n            <span ion-text color="primary" (click)="signup()">Sign up</span>\n          </span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the LoginPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var CardSettingPage = (function () {
    function CardSettingPage(nav, authService, toastCtrl, alertCtrl, loadingCtrl, navParams) {
        var _this = this;
        this.nav = nav;
        this.authService = authService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        authService.getCardSetting().take(1).subscribe(function (snapshot) {
            _this.number = snapshot.number;
            _this.exp = snapshot.exp;
            _this.cvv = snapshot.cvv;
        });
    }
    // save card settings
    CardSettingPage.prototype.save = function () {
        var _this = this;
        var exp = this.exp.split('/');
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        Stripe.card.createToken({
            number: this.number,
            exp_month: exp[0],
            exp_year: exp[1],
            cvc: this.cvv
        }, function (status, response) {
            loading.dismiss();
            // success
            if (status == 200) {
                // if nav from payment method selection
                if (_this.navParams.get('back')) {
                    _this.nav.pop();
                }
                else {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
                }
                _this.authService.updateCardSetting(_this.number, _this.exp, _this.cvv, response.id);
                var toast = _this.toastCtrl.create({
                    message: 'Your card setting has been updated',
                    duration: 3000,
                    position: 'middle'
                });
                toast.present();
            }
            else {
                // error
                var alert_1 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: response.error.message,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    CardSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-card-setting',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\card-setting\card-setting.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Card Setting</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="save()">Save</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-item>\n        <ion-label stacked>Card number</ion-label>\n        <ion-input type="text" [(ngModel)]="number" size="20"></ion-input>\n      </ion-item>\n    </ion-row>\n    <ion-row class="split-row">\n      <ion-col col-6>\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="exp" size="5" placeholder="Expiry(mm/yy)"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col col-6>\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="cvv" size="4" placeholder="CVV"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\card-setting\card-setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], CardSettingPage);
    return CardSettingPage;
}());

//# sourceMappingURL=card-setting.js.map

/***/ }),

/***/ 186:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 186;

/***/ }),

/***/ 230:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 230;

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
 Generated class for the RegisterPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(nav, authService, alertCtrl, loadingCtrl) {
        this.nav = nav;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    // process signup button
    RegisterPage.prototype.signup = function () {
        var _this = this;
        // require email, password, name
        if (!this.email || !this.password || !this.name) {
            var alert_1 = this.alertCtrl.create({
                message: 'Please provide email, name and password',
                buttons: ['OK']
            });
            return alert_1.present();
        }
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.authService.register(this.email, this.password, this.name).subscribe(function (authData) {
            loading.dismiss();
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }, function (error) {
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                message: error.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    RegisterPage.prototype.login = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\register\register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content class=" auth-bg">\n  <div class="auth-content">\n\n    <div class="light-bg">\n\n      <!-- Logo -->\n      <div padding text-center>\n        <div class="logo primary-bg">\n          <ion-icon name="ios-car" color="light"></ion-icon>\n        </div>\n        <h2 ion-text color="dark">\n          Firetaxi\n        </h2>\n        <p ion-text color="dark">Firebase taxi booking app</p>\n      </div>\n\n      <!-- Login form -->\n\n      <ion-list class="list-form" padding>\n\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="name" placeholder="Your name"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-input type="email" [(ngModel)]="email" placeholder="Email"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-input type="password" [(ngModel)]="password" placeholder="Password"></ion-input>\n        </ion-item>\n\n      </ion-list>\n\n      <div padding>\n        <button ion-button color="primary" block (click)="signup()">\n          Create account\n        </button>\n      </div>\n\n    </div>\n\n    <div padding=""></div>\n\n    <!-- Other links -->\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <span ion-text color="light">Forgot password?</span>\n        </ion-col>\n        <ion-col>\n          <span ion-text color="light">\n            Have account?\n            <span ion-text color="primary" (click)="login()">Login</span>\n          </span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\register\register.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlacesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_place_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_trip_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
 Generated class for the PlacesPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var PlacesPage = (function () {
    function PlacesPage(nav, placeService, geolocation, loadingCtrl, navParams, tripService) {
        this.nav = nav;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.tripService = tripService;
        // map id
        this.mapId = Math.random() + 'map';
        // search keyword
        this.keyword = '';
        // page loaded flag
        this.pageLoaded = false;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
    }
    // show search input
    PlacesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.pageLoaded = true;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.lat = resp.coords.latitude;
            _this.lon = resp.coords.longitude;
            _this.loadMap();
            _this.search();
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    // hide search input
    PlacesPage.prototype.ionViewWillLeave = function () {
        this.pageLoaded = false;
    };
    // load google map service
    PlacesPage.prototype.loadMap = function () {
        var latLng = new google.maps.LatLng(this.lat, this.lon);
        var map = new google.maps.Map(document.getElementById(this.mapId), {
            center: latLng
        });
        this.mapService = new google.maps.places.PlacesService(map);
    };
    // choose a place
    PlacesPage.prototype.selectPlace = function (place) {
        if (this.navParams.get('type') == 'origin') {
            this.tripService.setOrigin(place.vicinity, place.geometry.location.lat(), place.geometry.location.lng());
        }
        else {
            this.tripService.setDestination(place.vicinity, place.geometry.location.lat(), place.geometry.location.lng());
        }
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    // clear search input
    PlacesPage.prototype.clear = function () {
        this.keyword = '';
        this.search();
    };
    // search by address
    PlacesPage.prototype.search = function () {
        var _this = this;
        this.showLoading();
        this.mapService.nearbySearch({
            location: new google.maps.LatLng(this.lat, this.lon),
            keyword: this.keyword,
            radius: 50000
        }, function (results) {
            _this.hideLoading();
            _this.places = results;
        });
    };
    // calculate distance from a place to current position
    PlacesPage.prototype.calcDistance = function (place) {
        return this.placeService.calcCrow(place.geometry.location.lat, place.geometry.location.lng, this.lat, this.lon).toFixed(1);
    };
    PlacesPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    PlacesPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    // open map page
    PlacesPage.prototype.openMap = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */], { type: this.navParams.get('type') });
    };
    PlacesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-places',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\places\places.html"*/'<!--\n  Generated template for the PlacesPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="white">\n    <div class="search-bar">\n      <input type="text"\n             [(ngModel)]="keyword"\n             (change)="search($event)"\n             [hidden]="!pageLoaded"\n             autocorrect="off"\n             placeholder="Where are you going?">\n      <div class="close-btn" [hidden]="!keyword.length" (click)="clear()">\n        <ion-icon name="close"></ion-icon>\n      </div>\n    </div>\n    <ion-buttons end>\n      <button ion-button (click)="openMap()">\n        <ion-icon name="pin"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="common-bg">\n  <div id="{{ mapId }}" style="height: 0"></div>\n  <ion-list>\n    <ion-item *ngFor="let place of places" (click)="selectPlace(place)">\n      <ion-icon name="ios-pin-outline" item-left>\n      </ion-icon>\n      <span class="item-icon-label">\n        {{ calcDistance(place) }} km\n      </span>\n      <div>\n        <div class="bold">{{ place.name }}</div>\n        <span>{{ place.vicinity }}</span>\n      </div>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\places\places.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_place_service__["a" /* PlaceService */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__services_trip_service__["a" /* TripService */]])
    ], PlacesPage);
    return PlacesPage;
}());

//# sourceMappingURL=places.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_place_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_trip_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var MapPage = (function () {
    function MapPage(nav, geolocation, chRef, navParams, placeService, tripService) {
        this.nav = nav;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.navParams = navParams;
        this.placeService = placeService;
        this.tripService = tripService;
        // marker position on screen
        this.markerFromTop = 0;
        this.markerFromLeft = 0;
    }
    // Load map only after view is initialized
    MapPage.prototype.ionViewDidLoad = function () {
        this.loadMap();
        // set marker position in center of screen
        // minus marker's size
        this.markerFromTop = window.screen.height / 2 - 16;
        this.markerFromLeft = window.screen.width / 2 - 8;
    };
    MapPage.prototype.loadMap = function () {
        var _this = this;
        // set current location as map center
        this.geolocation.getCurrentPosition().then(function (resp) {
            var latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            _this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                zoomControl: false,
            });
            // get center's address
            _this.findPlace(latLng);
            _this.map.addListener('center_changed', function (event) {
                var center = _this.map.getCenter();
                _this.findPlace(center);
            });
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    // find address by LatLng
    MapPage.prototype.findPlace = function (latLng) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latLng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                _this.address = results[0];
                _this.chRef.detectChanges();
            }
        });
    };
    // choose address and go back to home page
    MapPage.prototype.selectPlace = function () {
        var address = this.placeService.formatAddress(this.address);
        if (this.navParams.get('type') == 'origin') {
            this.tripService.setOrigin(address.vicinity, address.location.lat, address.location.lng);
        }
        else {
            this.tripService.setDestination(address.vicinity, address.location.lat, address.location.lng);
        }
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-map',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\map\map.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="white">\n    <ion-title>{{ address ? address.formatted_address : \'\' }}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="selectPlace()">\n        <ion-icon name="md-checkmark"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div #map id="map"></div>\n  <img class="marker" src="assets/img/pin.png" alt=""\n       [ngStyle]="{top: markerFromTop + \'px\', left: markerFromLeft + \'px\'}">\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\map\map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_place_service__["a" /* PlaceService */], __WEBPACK_IMPORTED_MODULE_5__services_trip_service__["a" /* TripService */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentMethodPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__card_setting_card_setting__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_trip_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
 Generated class for the PaymentMethodPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var PaymentMethodPage = (function () {
    function PaymentMethodPage(nav, authService, tripService, loadingCtrl) {
        var _this = this;
        this.nav = nav;
        this.authService = authService;
        this.tripService = tripService;
        this.loadingCtrl = loadingCtrl;
        this.carNumber = null;
        var loading = loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        authService.getCardSetting().take(1).subscribe(function (snapshot) {
            loading.dismiss();
            if (snapshot) {
                _this.carNumber = snapshot.number;
            }
        });
    }
    // apply change method
    PaymentMethodPage.prototype.changeMethod = function (method) {
        this.tripService.setPaymentMethod(method);
        // go back
        this.nav.pop();
    };
    // add card
    PaymentMethodPage.prototype.addCard = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__card_setting_card_setting__["a" /* CardSettingPage */], { back: true });
    };
    PaymentMethodPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-method',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\payment-method\payment-method.html"*/'<!--\n  Generated template for the PaymentMethodPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Payment Methods</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list class="list-full-border" radio-group>\n    <ion-item>\n      <ion-label>\n        Cash\n        <p class="text-hint">Use cash</p>\n      </ion-label>\n      <ion-radio value="cash" (click)="changeMethod(\'cash\')"></ion-radio>\n    </ion-item>\n\n    <ion-item *ngIf="carNumber">\n      <ion-label>\n        Card\n        <p class="text-hint">{{ carNumber }}</p>\n      </ion-label>\n      <ion-radio value="card" (click)="changeMethod(\'card\')"></ion-radio>\n    </ion-item>\n\n  </ion-list>\n\n  <div padding *ngIf="!carNumber">\n    <button ion-button block color="primary" (click)="addCard()">Add payment method</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\payment-method\payment-method.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_4__services_trip_service__["a" /* TripService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], PaymentMethodPage);
    return PaymentMethodPage;
}());

//# sourceMappingURL=payment-method.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FindingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__driver_driver__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_trip_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_deal_service__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_constants__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
 Generated class for the FindingPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var FindingPage = (function () {
    function FindingPage(nav, tripService, dealService) {
        this.nav = nav;
        this.tripService = tripService;
        this.dealService = dealService;
        // get list available drivers
        this.drivers = this.tripService.getAvailableDrivers();
        // sort by driver distance and rating
        this.drivers = this.dealService.sortDriversList(this.drivers);
        if (this.drivers) {
            // make deal to first user
            this.makeDeal(0);
        }
    }
    // make deal to driver
    FindingPage.prototype.makeDeal = function (index) {
        var _this = this;
        var driver = this.drivers[index];
        var dealAccepted = false;
        if (driver) {
            driver.status = 'Bidding';
            this.dealService.getDriverDeal(driver.$key).take(1).subscribe(function (snapshot) {
                // if user is available
                if (snapshot.$value === null) {
                    // create a record
                    console.log(snapshot);
                    _this.dealService.makeDeal(driver.$key, _this.tripService.getOrigin(), _this.tripService.getDestination(), _this.tripService.getDistance(), _this.tripService.getFee(), _this.tripService.getCurrency(), _this.tripService.getNote(), _this.tripService.getPaymentMethod()).then(function () {
                        var sub = _this.dealService.getDriverDeal(driver.$key).subscribe(function (snap) {
                            // if record doesn't exist or is accepted
                            if (snap.$value === null || snap.status != __WEBPACK_IMPORTED_MODULE_6__services_constants__["b" /* DEAL_STATUS_PENDING */]) {
                                sub.unsubscribe();
                                // if deal has been cancelled
                                if (snap.$value === null) {
                                    _this.nextDriver(index);
                                }
                                else if (snap.status == __WEBPACK_IMPORTED_MODULE_6__services_constants__["a" /* DEAL_STATUS_ACCEPTED */]) {
                                    // if deal is accepted
                                    console.log('accepted', snap.tripId);
                                    dealAccepted = true;
                                    _this.drivers = [];
                                    _this.tripService.setId(snap.tripId);
                                    // go to user page
                                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__driver_driver__["a" /* DriverPage */]);
                                }
                            }
                        });
                        // if timeout
                        /*
                        setTimeout(() => {
                          if (!dealAccepted) {
                            sub.unsubscribe();
                            // remove record
                            this.dealService.removeDeal(driver.$key);
                            // make deal to other user
                            this.nextDriver(index);
                          }
                        }, DEAL_TIMEOUT);
                        */
                    });
                }
                else {
                    _this.nextDriver(index);
                }
            });
        }
        else {
            // show error & try again button
            console.log('No user found');
        }
    };
    // make deal to next driver
    FindingPage.prototype.nextDriver = function (index) {
        this.drivers.splice(index, 1);
        this.makeDeal(index);
    };
    FindingPage.prototype.goBack = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    FindingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-finding',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\finding\finding.html"*/'<!--\n  Generated template for the FindingPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Finding you a driver</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="primary-bg">\n  <!-- Green circle with marker icon in the center -->\n  <div padding text-center="">\n    <div class="location-animation circle">\n      <ion-icon name="pin" color="light"></ion-icon>\n    </div>\n  </div>\n\n  <h2 ion-text color="light" text-center [hidden]="drivers.length">\n    No driver found\n  </h2>\n\n  <ion-list class="list-no-border list-drivers" padding>\n    <ion-item *ngFor="let driver of drivers; let i = index">\n      <span>{{ driver.name }}</span>\n\n      <img src="assets/img/loading.gif" alt="" [hidden]="i" item-end>\n      <span item-end [hidden]="!i">\n        Waiting\n      </span>\n    </ion-item>\n  </ion-list>\n\n  <div class="align-bottom" padding>\n    <button class="border-button" ion-button block color="primary" (click)="goBack()"\n            [hidden]="drivers.length">TRY AGAIN\n    </button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\finding\finding.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_trip_service__["a" /* TripService */], __WEBPACK_IMPORTED_MODULE_4__services_deal_service__["a" /* DealService */]])
    ], FindingPage);
    return FindingPage;
}());

//# sourceMappingURL=finding.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DriverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_driver_service__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tracking_tracking__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_trip_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
 Generated class for the DriverPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var DriverPage = (function () {
    function DriverPage(nav, driverService, tripService) {
        var _this = this;
        this.nav = nav;
        this.driverService = driverService;
        this.tripService = tripService;
        var tripId = this.tripService.getId();
        // get current trip
        this.tripService.getTrip(tripId).take(1).subscribe(function (snapshot) {
            _this.trip = snapshot;
            // get driver from trip
            _this.driverService.getDriver(snapshot.driverId).take(1).subscribe(function (snap) {
                _this.driver = snap;
            });
        });
        // after 5 seconds, go to trcking page
        this.navTimeout = setTimeout(function () {
            _this.track();
        }, 5000);
    }
    DriverPage.prototype.ionViewWillLeave = function () {
        clearTimeout(this.navTimeout);
    };
    DriverPage.prototype.track = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__tracking_tracking__["a" /* TrackingPage */]);
    };
    // make array with range is n
    DriverPage.prototype.range = function (n) {
        return new Array(Math.round(n));
    };
    DriverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-driver',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\driver\driver.html"*/'<!--\n  Generated template for the DriverPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Driver found</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="driver-top primary-bg light" text-center>\n    <div class="driver-picture">\n      <img class="circle" src="{{ (driver)?.photoURL }}" alt=""/>\n    </div>\n\n    <h3 ion-text no-margin>{{ (driver)?.name }} </h3>\n    <div class="stars" *ngIf="driver">\n      <ion-icon name="md-star" color="yellow" *ngFor="let star of range((driver)?.rating)"></ion-icon>\n      <ion-icon name="ios-star-outline" color="gray" *ngFor="let star of range(4.9 - (driver)?.rating)"></ion-icon>\n    </div>\n    <div>\n      {{ (driver)?.plate }} &middot; {{ (driver)?.brand }}\n    </div>\n  </div>\n\n  <div class="dark-bg" padding>\n    <span ion-text color="light" class="pull-left">\n      Fare: <strong>{{(trip)?.currency }}{{(trip)?.fee }}</strong>\n    </span>\n    <ion-icon ion-text color="light" class="pull-right" name="logo-usd"></ion-icon>\n    <div class="clear"></div>\n  </div>\n\n  <ion-list class="list-no-border address-block">\n    <ion-item>\n      <div item-left text-center>\n        <span class="round"></span>\n      </div>\n      <div>{{ (trip)?.origin.vicinity }}</div>\n    </ion-item>\n    <ion-item>\n      <div item-left text-center>\n        <ion-icon name="pin" color="danger"></ion-icon>\n      </div>\n      <div>{{ (trip)?.destination.vicinity }}</div>\n    </ion-item>\n  </ion-list>\n\n  <div class="align-bottom" padding>\n    <button ion-button block color="primary" (click)="track()">Next</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\driver\driver.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_driver_service__["a" /* DriverService */], __WEBPACK_IMPORTED_MODULE_4__services_trip_service__["a" /* TripService */]])
    ], DriverPage);
    return DriverPage;
}());

//# sourceMappingURL=driver.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrackingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_driver_service__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_trip_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_constants__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_place_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modal_rating_modal_rating__ = __webpack_require__(296);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
 Generated class for the TrackingPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var TrackingPage = (function () {
    function TrackingPage(nav, driverService, platform, tripService, placeService, modalCtrl) {
        this.nav = nav;
        this.driverService = driverService;
        this.platform = platform;
        this.tripService = tripService;
        this.placeService = placeService;
        this.modalCtrl = modalCtrl;
        // map height
        this.mapHeight = 480;
    }
    TrackingPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var tripId = this.tripService.getId();
        this.tripService.getTrip(tripId).take(1).subscribe(function (snapshot) {
            _this.trip = snapshot;
            _this.driverService.getDriver(snapshot.driverId).take(1).subscribe(function (snap) {
                console.log(snap);
                _this.driver = snap;
                _this.watchTrip(tripId);
                // init map
                _this.loadMap();
            });
        });
    };
    TrackingPage.prototype.ionViewWillLeave = function () {
        //this.tripSubscription.unsubscribe();
        clearInterval(this.driverTracking);
    };
    TrackingPage.prototype.watchTrip = function (tripId) {
        var _this = this;
        this.tripSubscription = this.tripService.getTrip(tripId).subscribe(function (snapshot) {
            _this.tripStatus = snapshot.status;
            // if trip has been finished
            if (_this.tripStatus == __WEBPACK_IMPORTED_MODULE_5__services_constants__["f" /* TRIP_STATUS_FINISHED */]) {
                _this.tripSubscription.unsubscribe();
                _this.showRatingModal();
            }
        });
    };
    TrackingPage.prototype.showRatingModal = function () {
        var _this = this;
        console.log(this.trip, this.driver);
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modal_rating_modal_rating__["a" /* ModalRatingPage */], {
            trip: this.trip,
            driver: this.driver
        });
        modal.onDidDismiss(function (data) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        });
        modal.present();
    };
    TrackingPage.prototype.loadMap = function () {
        var latLng = new google.maps.LatLng(this.trip.origin.location.lat, this.trip.origin.location.lng);
        var mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false
        };
        this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        // get ion-view height
        var viewHeight = window.screen.height - 44; // minus nav bar
        // get info block height
        var infoHeight = document.getElementsByClassName('tracking-info')[0].scrollHeight;
        this.mapHeight = viewHeight - infoHeight;
        new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: 'assets/img/pin.png'
        });
        this.trackDriver();
    };
    // make array with range is n
    TrackingPage.prototype.range = function (n) {
        return new Array(Math.round(n));
    };
    TrackingPage.prototype.trackDriver = function () {
        var _this = this;
        this.showDriverOnMap();
        this.driverTracking = setInterval(function () {
            _this.marker.setMap(null);
            _this.showDriverOnMap();
        }, __WEBPACK_IMPORTED_MODULE_5__services_constants__["d" /* POSITION_INTERVAL */]);
        console.log(__WEBPACK_IMPORTED_MODULE_5__services_constants__["d" /* POSITION_INTERVAL */]);
    };
    // show user on map
    TrackingPage.prototype.showDriverOnMap = function () {
        var _this = this;
        // get user's position
        this.driverService.getDriverPosition(this.placeService.getLocality(), this.driver.type, this.driver.$key).take(1).subscribe(function (snapshot) {
            // create or update
            var latLng = new google.maps.LatLng(snapshot.lat, snapshot.lng);
            var angle = _this.driverService.getIconWithAngle(snapshot);
            if (_this.tripStatus == __WEBPACK_IMPORTED_MODULE_5__services_constants__["g" /* TRIP_STATUS_GOING */]) {
                console.log(_this.tripStatus);
                _this.map.setCenter(latLng);
            }
            // show vehicle to map
            _this.marker = new google.maps.Marker({
                map: _this.map,
                position: latLng,
                icon: {
                    url: 'assets/img/icon/' + _this.tripService.getIcon() + angle + '.png',
                    size: new google.maps.Size(32, 32),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(16, 16),
                    scaledSize: new google.maps.Size(32, 32)
                },
            });
        });
    };
    TrackingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tracking',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\tracking\tracking.html"*/'<!--\n  Generated template for the TrackingPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Driver on the way</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <!-- Show map here -->\n  <div id="map" [ngStyle]="{height: mapHeight + \'px\'}"></div>\n\n  <ion-list class="list-no-border tracking-info">\n    <ion-item text-center>\n      <h2>Your driver is on the way</h2>\n      <p ion-text color="dark" margin-top>\n        {{ (driver)?.plate }} &middot; {{ (driver)?.brand }}\n      </p>\n    </ion-item>\n    <ion-item class="no-border">\n      <img class="circle icon pull-left" src="{{ (driver)?.photoURL }}" item-left alt=""/>\n      <div class="item-content">\n        <div>\n          <div>{{ (driver)?.name }}</div>\n          <div class="stars" *ngIf="driver">\n            <ion-icon name="md-star" color="yellow" *ngFor="let star of range((driver)?.rating)"></ion-icon>\n            <ion-icon name="ios-star-outline" color="gray"\n                      *ngFor="let star of range(4.9 - (driver)?.rating)"></ion-icon>\n          </div>\n        </div>\n\n        <div class="action-icons">\n          <a href="tel:{{ (driver)?.phoneNumber }}">\n            <ion-icon name="call" color="secondary"></ion-icon>\n          </a>\n          <a href="sms:{{ (driver)?.phoneNumber }}">\n            <ion-icon name="mail" color="secondary"></ion-icon>\n          </a>\n        </div>\n      </div>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\tracking\tracking.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_driver_service__["a" /* DriverService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__services_trip_service__["a" /* TripService */], __WEBPACK_IMPORTED_MODULE_6__services_place_service__["a" /* PlaceService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], TrackingPage);
    return TrackingPage;
}());

//# sourceMappingURL=tracking.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalRatingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trip_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
 Generated class for the ModalRatingPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var ModalRatingPage = (function () {
    function ModalRatingPage(nav, navParams, tripService, viewCtrl, app) {
        this.nav = nav;
        this.navParams = navParams;
        this.tripService = tripService;
        this.viewCtrl = viewCtrl;
        this.app = app;
        // rating
        this.rating = 0;
        // get params from navParams
        this.driver = navParams.get('driver');
        this.trip = navParams.get('trip');
    }
    // call when click to stars
    ModalRatingPage.prototype.rate = function (star) {
        this.rating = star;
    };
    // submit rating
    ModalRatingPage.prototype.submit = function () {
        var _this = this;
        this.tripService.rateTrip(this.trip.$key, this.rating).then(function () {
            _this.viewCtrl.dismiss();
        });
    };
    ModalRatingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-modal-rating',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\modal-rating\modal-rating.html"*/'<!--\n  Generated template for the ModalRatingPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="white">\n    <ion-title>Finished</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="primary-bg">\n  <div class="light-bg" padding text-center>\n    <h5 ion-text color="gray">Fare</h5>\n    <h2 ion-text color="primary">\n      {{ trip.currency }} {{ trip.fee }}\n    </h2>\n  </div>\n  <div padding text-center>\n    <h1 ion-text color="white">\n      Thank you!\n    </h1>\n    <div class="driver-picture">\n      <img class="circle" src="{{ (driver)?.photoURL }}" alt=""/>\n    </div>\n    <div padding-top>\n      <span ion-text color="light">Please rate my service</span>\n      <div class="stars">\n        <ion-icon name="md-star" color="{{ (rating < 1) ? \'light\' : \'yellow\' }}" (click)="rate(1)"></ion-icon>\n        <ion-icon name="md-star" color="{{ (rating < 2) ? \'light\' : \'yellow\' }}" (click)="rate(2)"></ion-icon>\n        <ion-icon name="md-star" color="{{ (rating < 3) ? \'light\' : \'yellow\' }}" (click)="rate(3)"></ion-icon>\n        <ion-icon name="md-star" color="{{ (rating < 4) ? \'light\' : \'yellow\' }}" (click)="rate(4)"></ion-icon>\n        <ion-icon name="md-star" color="{{ (rating < 5) ? \'light\' : \'yellow\' }}" (click)="rate(5)"></ion-icon>\n      </div>\n    </div>\n  </div>\n  <div class="align-bottom" padding [hidden]="rating < 1">\n    <button ion-button block color="light" (click)="submit()">\n      SUBMIT\n    </button>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\modal-rating\modal-rating.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_trip_service__["a" /* TripService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], ModalRatingPage);
    return ModalRatingPage;
}());

//# sourceMappingURL=modal-rating.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database_database__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DealService = (function () {
    function DealService(db, authService) {
        this.db = db;
        this.authService = authService;
    }
    // sort driver by rating & distance
    DealService.prototype.sortDriversList = function (drivers) {
        return drivers.sort(function (a, b) {
            return (a.rating - a.distance / 5) - (b.rating - b.distance / 5);
        });
    };
    // make deal to driver
    DealService.prototype.makeDeal = function (driverId, origin, destination, distance, fee, currency, note, paymentMethod) {
        var user = this.authService.getUserData();
        return this.db.object('deals/' + driverId).set({
            passengerId: user.uid,
            currency: currency,
            origin: origin,
            destination: destination,
            distance: distance,
            fee: fee,
            note: note,
            paymentMethod: paymentMethod,
            status: __WEBPACK_IMPORTED_MODULE_2__constants__["b" /* DEAL_STATUS_PENDING */],
            createdAt: Date.now()
        });
    };
    // get deal by driverId
    DealService.prototype.getDriverDeal = function (driverId) {
        return this.db.object('deals/' + driverId);
    };
    // remove deal
    DealService.prototype.removeDeal = function (driverId) {
        return this.db.object('deals/' + driverId).remove();
    };
    DealService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]])
    ], DealService);
    return DealService;
}());

//# sourceMappingURL=deal-service.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingService = (function () {
    function SettingService(db, storage) {
        this.db = db;
        this.storage = storage;
    }
    SettingService.prototype.getPrices = function () {
        return this.db.object('master_settings/prices');
    };
    SettingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], SettingService);
    return SettingService;
}());

//# sourceMappingURL=setting-service.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database_database__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__place__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripService = (function () {
    function TripService(db, authService) {
        this.db = db;
        this.authService = authService;
        this.paymentMethod = 'cash';
        this.availableDrivers = [];
    }
    TripService.prototype.getAll = function () {
        return this.trips;
    };
    TripService.prototype.setId = function (id) {
        return this.id = id;
    };
    TripService.prototype.getId = function () {
        return this.id;
    };
    TripService.prototype.setCurrency = function (currency) {
        return this.currency = currency;
    };
    TripService.prototype.getCurrency = function () {
        return this.currency;
    };
    TripService.prototype.setOrigin = function (vicinity, lat, lng) {
        var place = new __WEBPACK_IMPORTED_MODULE_2__place__["a" /* Place */](vicinity, lat, lng);
        return this.origin = place.getFormatted();
    };
    TripService.prototype.getOrigin = function () {
        return this.origin;
    };
    TripService.prototype.setDestination = function (vicinity, lat, lng) {
        var place = new __WEBPACK_IMPORTED_MODULE_2__place__["a" /* Place */](vicinity, lat, lng);
        return this.destination = place.getFormatted();
    };
    TripService.prototype.getDestination = function () {
        return this.destination;
    };
    TripService.prototype.setDistance = function (distance) {
        return this.distance = distance;
    };
    TripService.prototype.getDistance = function () {
        return this.distance;
    };
    TripService.prototype.setFee = function (fee) {
        return this.fee = fee;
    };
    TripService.prototype.getFee = function () {
        return this.fee;
    };
    TripService.prototype.setNote = function (note) {
        return this.note = note;
    };
    TripService.prototype.getNote = function () {
        return this.note;
    };
    TripService.prototype.setPaymentMethod = function (method) {
        return this.paymentMethod = method;
    };
    TripService.prototype.getPaymentMethod = function () {
        return this.paymentMethod;
    };
    TripService.prototype.setVehicle = function (vehicle) {
        return this.vehicle = vehicle;
    };
    TripService.prototype.getVehicle = function () {
        return this.vehicle;
    };
    TripService.prototype.setIcon = function (icon) {
        return this.icon = icon;
    };
    TripService.prototype.getIcon = function () {
        return this.icon;
    };
    TripService.prototype.setAvailableDrivers = function (vehicles) {
        console.log(vehicles);
        this.availableDrivers = vehicles;
    };
    TripService.prototype.getAvailableDrivers = function () {
        return this.availableDrivers;
    };
    TripService.prototype.getTrip = function (id) {
        return this.db.object('trips/' + id);
    };
    TripService.prototype.getTrips = function () {
        var user = this.authService.getUserData();
        console.log(user);
        return this.db.list('trips', {
            query: {
                orderByChild: 'passengerId',
                equalTo: user.uid
            }
        });
    };
    TripService.prototype.rateTrip = function (tripId, stars) {
        return this.db.object('trips/' + tripId).update({
            rating: stars
        });
    };
    TripService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]])
    ], TripService);
    return TripService;
}());

//# sourceMappingURL=trip-service.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__places_places__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__payment_method_payment_method__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__finding_finding__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_place_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_setting_service__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_driver_service__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_trip_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_constants__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_Rx__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/*
 Generated class for the HomePage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var HomePage = (function () {
    function HomePage(nav, platform, alertCtrl, placeService, geolocation, chRef, loadingCtrl, settingService, tripService, driverService) {
        this.nav = nav;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.loadingCtrl = loadingCtrl;
        this.settingService = settingService;
        this.tripService = tripService;
        this.driverService = driverService;
        // map id
        this.mapId = Math.random() + 'map';
        // map height
        this.mapHeight = 480;
        // show - hide modal bg
        this.showModalBg = false;
        // show vehicles flag
        this.showVehicles = false;
        // list vehicles
        this.vehicles = [];
        // Note to user
        this.note = '';
        // distance between origin and destination
        this.distance = 0;
        // payment method
        this.paymentMethod = 'cash';
        // active drivers list
        this.activeDrivers = [];
        // list of user markers on the map
        this.driverMarkers = [];
        this.origin = tripService.getOrigin();
        this.destination = tripService.getDestination();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        // on view ready, start loading map
        this.loadMap();
    };
    HomePage.prototype.ionViewWillLeave = function () {
        // stop tracking driver
        clearInterval(this.driverTracking);
    };
    // get current payment method from service
    HomePage.prototype.getPaymentMethod = function () {
        this.paymentMethod = this.tripService.getPaymentMethod();
        return this.paymentMethod;
    };
    // toggle active vehicle
    HomePage.prototype.chooseVehicle = function (index) {
        for (var i = 0; i < this.vehicles.length; i++) {
            this.vehicles[i].active = (i == index);
            // choose this vehicle type
            if (i == index) {
                this.tripService.setVehicle(this.vehicles[i]);
                this.currentVehicle = this.vehicles[i];
            }
        }
        // start tracking new driver type
        this.trackDrivers();
        this.toggleVehicles();
    };
    // load map
    HomePage.prototype.loadMap = function () {
        var _this = this;
        this.showLoading();
        // get current location
        return this.geolocation.getCurrentPosition().then(function (resp) {
            var latLng;
            if (_this.origin) {
                // set map center as origin
                latLng = new google.maps.LatLng(_this.origin.location.lat, _this.origin.location.lng);
            }
            else {
                // set map center as current location
                latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            }
            // debug GPS
            // this.deb = latLng.lat() + ',' + latLng.lng();
            _this.map = new google.maps.Map(document.getElementById(_this.mapId), {
                zoom: 15,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                zoomControl: false,
                streetViewControl: false,
            });
            var directionService = new google.maps.DirectionsService(_this.map);
            // get ion-view height, 44 is navbar height
            _this.mapHeight = window.screen.height - 44;
            // find map center address
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': _this.map.getCenter() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (!_this.origin) {
                        // set map center as origin
                        _this.origin = _this.placeService.formatAddress(results[0]);
                        _this.tripService.setOrigin(_this.origin.vicinity, _this.origin.location.lat, _this.origin.location.lng);
                        _this.setOrigin();
                        _this.chRef.detectChanges();
                    }
                    else {
                        _this.setOrigin();
                    }
                    // save locality
                    var locality_1 = _this.placeService.setLocalityFromGeocoder(results);
                    console.log('locality', locality_1);
                    // load list vehicles
                    _this.settingService.getPrices().subscribe(function (snapshot) {
                        var obj = snapshot[locality_1] ? snapshot[locality_1] : snapshot.default;
                        _this.currency = obj.currency;
                        _this.tripService.setCurrency(_this.currency);
                        // calculate price
                        Object.keys(obj.vehicles).forEach(function (id) {
                            obj.vehicles[id].id = id;
                            _this.vehicles.push(obj.vehicles[id]);
                        });
                        // google map direction service
                        if (_this.destination) {
                            directionService.route({
                                origin: new google.maps.LatLng(_this.origin.location.lat, _this.origin.location.lng),
                                destination: new google.maps.LatLng(_this.destination.location.lat, _this.destination.location.lng),
                                travelMode: 'DRIVING'
                            }, function (result) {
                                _this.distance = result.routes[0].legs[0].distance.value;
                                for (var i = 0; i < _this.vehicles.length; i++) {
                                    _this.vehicles[i].fee = _this.distance * _this.vehicles[i].price / 1000;
                                    _this.vehicles[i].fee = _this.vehicles[i].fee.toFixed(2);
                                }
                            });
                        }
                        // set first device as default
                        _this.vehicles[0].active = true;
                        _this.currentVehicle = _this.vehicles[0];
                        _this.locality = locality_1;
                        _this.trackDrivers();
                    });
                }
            });
            // add destination to map
            if (_this.destination) {
                new google.maps.Marker({
                    map: _this.map,
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(_this.destination.location.lat, _this.destination.location.lng)
                });
            }
            _this.hideLoading();
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    // Show note popup when click to 'Notes to user'
    HomePage.prototype.showNotePopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Notes to user',
            message: "",
            inputs: [
                {
                    name: 'note',
                    placeholder: 'Note'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        _this.note = data;
                        _this.tripService.setNote(data);
                        console.log('Saved clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    ;
    // go to next view when the 'Book' button is clicked
    HomePage.prototype.book = function () {
        // store detail
        this.tripService.setAvailableDrivers(this.activeDrivers);
        this.tripService.setDistance(this.distance);
        this.tripService.setFee(this.currentVehicle.fee);
        this.tripService.setIcon(this.currentVehicle.icon);
        this.tripService.setNote(this.note);
        // this.tripService.setPaymentMethod('');
        // go to finding page
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__finding_finding__["a" /* FindingPage */]);
    };
    // choose origin place
    HomePage.prototype.chooseOrigin = function () {
        // go to places page
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__places_places__["a" /* PlacesPage */], { type: 'origin' });
    };
    // choose destination place
    HomePage.prototype.chooseDestination = function () {
        // go to places page
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__places_places__["a" /* PlacesPage */], { type: 'destination' });
    };
    // choose payment method
    HomePage.prototype.choosePaymentMethod = function () {
        // go to payment method page
        this.nav.push(__WEBPACK_IMPORTED_MODULE_4__payment_method_payment_method__["a" /* PaymentMethodPage */]);
    };
    // add origin marker to map
    HomePage.prototype.setOrigin = function () {
        // add origin and destination marker
        var latLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
        new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: 'assets/img/pin.png'
        });
        // set map center to origin address
        this.map.setCenter(latLng);
    };
    HomePage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    HomePage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    // show or hide vehicles
    HomePage.prototype.toggleVehicles = function () {
        this.showVehicles = !this.showVehicles;
        this.showModalBg = (this.showVehicles == true);
    };
    // track drivers
    HomePage.prototype.trackDrivers = function () {
        var _this = this;
        this.showDriverOnMap(this.locality);
        clearInterval(this.driverTracking);
        this.driverTracking = setInterval(function () {
            _this.showDriverOnMap(_this.locality);
        }, __WEBPACK_IMPORTED_MODULE_10__services_constants__["d" /* POSITION_INTERVAL */]);
    };
    // show drivers on map
    HomePage.prototype.showDriverOnMap = function (locality) {
        var _this = this;
        // get active drivers
        this.driverService.getActiveDriver(locality, this.currentVehicle.id).take(1).subscribe(function (snapshot) {
            console.log('refresh vehicles');
            // clear vehicles
            _this.clearDrivers();
            // only show near vehicle
            snapshot.forEach(function (vehicle) {
                // only show vehicle which has last active < 30 secs & distance < 5km
                var distance = _this.placeService.calcCrow(vehicle.lat, vehicle.lng, _this.origin.location.lat, _this.origin.location.lng);
                if (distance < __WEBPACK_IMPORTED_MODULE_10__services_constants__["e" /* SHOW_VEHICLES_WITHIN */]
                    && Date.now() - vehicle.last_active < __WEBPACK_IMPORTED_MODULE_10__services_constants__["h" /* VEHICLE_LAST_ACTIVE_LIMIT */]) {
                    // create or update
                    var latLng = new google.maps.LatLng(vehicle.lat, vehicle.lng);
                    var angle = _this.driverService.getIconWithAngle(vehicle);
                    var marker = new google.maps.Marker({
                        map: _this.map,
                        position: latLng,
                        icon: {
                            url: 'assets/img/icon/' + _this.currentVehicle.icon + angle + '.png',
                            size: new google.maps.Size(32, 32),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(16, 16),
                            scaledSize: new google.maps.Size(32, 32)
                        },
                    });
                    // add vehicle and marker to the list
                    vehicle.distance = distance;
                    _this.driverMarkers.push(marker);
                    _this.activeDrivers.push(vehicle);
                }
                else {
                    console.log('This vehicle is too far');
                }
            });
        });
    };
    // clear expired drivers on the map
    HomePage.prototype.clearDrivers = function () {
        this.activeDrivers = [];
        this.driverMarkers.forEach(function (vehicle) {
            vehicle.setMap(null);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\home\home.html"*/'<!--\n  Generated template for the HomePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>ionTaxi</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <!-- Show map here -->\n  <div id="{{ mapId }}" [ngStyle]="{height: mapHeight + \'px\'}"></div>\n\n  <!--Choose origin and destination places-->\n  <div class="map-overlay">\n    <ion-list class="list-no-border card address-block">\n      <ion-item (click)="chooseOrigin()">\n        <div item-left text-center>\n          <span class="round"></span>\n        </div>\n        <div>{{ origin ? origin.vicinity : \'\' }}</div>\n      </ion-item>\n      <ion-item (click)="chooseDestination()">\n        <div item-left text-center>\n          <ion-icon name="pin" color="danger"></ion-icon>\n        </div>\n        <div>{{ destination ? destination.vicinity : \'\' }}</div>\n      </ion-item>\n    </ion-list>\n\n    <ion-grid class="common-bg" [hidden]="!destination">\n      <ion-row>\n        <ion-col col-4 (click)="choosePaymentMethod()">\n          <ion-icon name="ios-cash-outline" color="gray"></ion-icon>\n          <span ion-text color="gray">{{ getPaymentMethod() }}</span>\n        </ion-col>\n        <ion-col col-4 (click)="showNotePopup()">\n          <ion-icon name="ios-create-outline" color="gray"></ion-icon>\n          <span ion-text color="gray">Note</span>\n        </ion-col>\n        <ion-col col-4>\n          <ion-icon name="ios-pricetag-outline" color="gray"></ion-icon>\n          <span ion-text color="gray">Promo</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="align-bottom" padding [hidden]="showModalBg">\n    <div class="vehicle-collapsed round" *ngFor="let vehicle of vehicles">\n      <div class="bg light-bg round" *ngIf="vehicle.active"></div>\n      <ion-item class="vehicle" *ngIf="vehicle.active" (click)="toggleVehicles()">\n        <img src="assets/img/icon/{{ vehicle.icon }}.svg" alt="" item-start>\n        <span class="name">{{ vehicle.name }}</span>\n        <span class="fee" item-end>{{ currency }}{{ vehicle.fee }}</span>\n      </ion-item>\n    </div>\n\n    <button ion-button block color="primary" [hidden]="destination" (click)="chooseDestination()">CHOOSE YOUR DROP-OFF\n    </button>\n    <button ion-button block color="primary" [hidden]="!destination" (click)="book()">BOOK</button>\n  </div>\n</ion-content>\n\n<div class="modal-bg" [hidden]="!showModalBg">\n  <div class="vehicles" [hidden]="!showVehicles" padding>\n    <ion-list class="round list-full-border">\n      <ion-item class="vehicle" *ngFor="let vehicle of vehicles; let i = index" [ngClass]="{\'active\': vehicle.active}"\n                (click)="chooseVehicle(i)">\n        <img src="assets/img/icon/{{ vehicle.icon }}.svg" alt="" item-start>\n        <span class="name">{{ vehicle.name }}</span>\n        <span class="fee" item-end>{{ currency }}{{ vehicle.fee }}</span>\n      </ion-item>\n    </ion-list>\n    <div text-center>\n      <button ion-button class="border-btn round-btn" color="dark" (click)="toggleVehicles()">\n        <ion-icon name="ios-arrow-down"></ion-icon>\n      </button>\n    </div>\n  </div>\n</div>\n\n<!--debug-->\n<span class="debug">{{ deb }}</span>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__services_place_service__["a" /* PlaceService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__services_setting_service__["a" /* SettingService */],
            __WEBPACK_IMPORTED_MODULE_9__services_trip_service__["a" /* TripService */], __WEBPACK_IMPORTED_MODULE_8__services_driver_service__["a" /* DriverService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__constants__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AuthService = (function () {
    function AuthService(afAuth, db, storage) {
        this.afAuth = afAuth;
        this.db = db;
        this.storage = storage;
    }
    // get current user data from firebase
    AuthService.prototype.getUserData = function () {
        return this.afAuth.auth.currentUser;
    };
    // get passenger by id
    AuthService.prototype.getUser = function (id) {
        return this.db.object('passengers/' + id);
    };
    // login by email and password
    AuthService.prototype.login = function (email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    };
    // login with facebook
    AuthService.prototype.loginWithFacebook = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].create(function (observer) {
            _this.afAuth.auth.signInWithPopup(new __WEBPACK_IMPORTED_MODULE_5_firebase_app__["auth"].FacebookAuthProvider()).then(function (result) {
                _this.createUserIfNotExist(result.user);
                observer.next();
            }).catch(function (error) {
                if (error) {
                    observer.error(error);
                }
            });
        });
    };
    // login with google
    AuthService.prototype.loginWithGoogle = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].create(function (observer) {
            return _this.afAuth.auth.signInWithPopup(new __WEBPACK_IMPORTED_MODULE_5_firebase_app__["auth"].GoogleAuthProvider()).then(function (result) {
                _this.createUserIfNotExist(result.user);
                observer.next();
            }).catch(function (error) {
                if (error) {
                    observer.error(error);
                }
            });
        });
    };
    AuthService.prototype.logout = function () {
        return this.afAuth.auth.signOut();
    };
    // register new account
    AuthService.prototype.register = function (email, password, name) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].create(function (observer) {
            _this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(function (authData) {
                authData.name = name;
                // update passenger object
                _this.updateUserProfile(authData);
                observer.next();
            }).catch(function (error) {
                if (error) {
                    observer.error(error);
                }
            });
        });
    };
    // update user display name and photo
    AuthService.prototype.updateUserProfile = function (user) {
        var name = user.name ? user.name : user.email;
        var photoUrl = user.photoURL ? user.photoURL : __WEBPACK_IMPORTED_MODULE_7__constants__["c" /* DEFAULT_AVATAR */];
        this.getUserData().updateProfile({
            displayName: name,
            photoURL: photoUrl
        });
        // create or update passenger
        this.db.object('passengers/' + user.uid).update({
            name: name,
            photoURL: photoUrl,
            email: user.email,
            phoneNumber: user.phoneNumber ? user.phoneNumber : ''
        });
    };
    // create new user if not exist
    AuthService.prototype.createUserIfNotExist = function (user) {
        var _this = this;
        // check if user does not exist
        this.getUser(user.uid).take(1).subscribe(function (snapshot) {
            if (snapshot.$value === null) {
                // update passenger object
                _this.updateUserProfile(user);
            }
        });
    };
    // update card setting
    AuthService.prototype.updateCardSetting = function (number, exp, cvv, token) {
        var user = this.getUserData();
        this.db.object('passengers/' + user.uid + '/card').update({
            number: number,
            exp: exp,
            cvv: cvv,
            token: token
        });
    };
    // get card setting
    AuthService.prototype.getCardSetting = function () {
        var user = this.getUserData();
        return this.db.object('passengers/' + user.uid + '/card');
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notification_service__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
 Generated class for the NotificationPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var NotificationPage = (function () {
    function NotificationPage(nav, notificationService) {
        this.nav = nav;
        this.notificationService = notificationService;
        this.notifications = notificationService.getAll();
    }
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notification',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\notification\notification.html"*/'<!--\n  Generated template for the NotificationPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Notification</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list class="list-full-border">\n    <ion-item *ngFor="let noti of notifications">\n      <ion-icon name="ios-mail-outline" color="primary" item-left></ion-icon>\n      <div [ngClass]="{\'bold\': !noti.read}">{{ noti.title }}</div>\n      <span>{{ noti.createdAt }}</span>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\notification\notification.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_notification_service__["a" /* NotificationService */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_notifications__ = __webpack_require__(857);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationService = (function () {
    function NotificationService() {
        this.notifications = __WEBPACK_IMPORTED_MODULE_1__mock_notifications__["a" /* NOTIFICATIONS */];
    }
    NotificationService.prototype.getAll = function () {
        return this.notifications;
    };
    NotificationService.prototype.getItem = function (id) {
        for (var i = 0; i < this.notifications.length; i++) {
            if (this.notifications[i].id === parseInt(id)) {
                return this.notifications[i];
            }
        }
        return null;
    };
    NotificationService.prototype.remove = function (item) {
        this.notifications.splice(this.notifications.indexOf(item), 1);
    };
    NotificationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], NotificationService);
    return NotificationService;
}());

//# sourceMappingURL=notification-service.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
 Generated class for the SupportPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var SupportPage = (function () {
    function SupportPage(nav) {
        this.nav = nav;
    }
    SupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-support',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\support\support.html"*/'<!--\n  Generated template for the SupportPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Support</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="support">\n  <div class="feedback-form padding">\n    <textarea name="feed_back" placeholder="Type your feedback here"></textarea>\n    <button ion-button block color="primary">SEND</button>\n    <div text-center>OR</div>\n    <button ion-button button block color="dark">CALL US</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\support\support.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], SupportPage);
    return SupportPage;
}());

//# sourceMappingURL=support.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trip_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trip_detail_trip_detail__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var TripsPage = (function () {
    function TripsPage(nav, tripService) {
        var _this = this;
        this.nav = nav;
        this.tripService = tripService;
        this.tripService.getTrips().take(1).subscribe(function (snapshot) {
            _this.trips = snapshot.reverse();
        });
    }
    TripsPage.prototype.viewTrip = function (trip) {
        console.log(trip);
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__trip_detail_trip_detail__["a" /* TripDetailPage */], {
            trip: trip
        });
    };
    TripsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trips',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\trips\trips.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>History</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list class="trip-list">\n    <ion-item *ngFor="let trip of trips" (click)="viewTrip(trip)">\n      <div text-right>\n        {{ trip.pickedUpAt | amDateFormat: \'YYYY-MM-DD HH:mm\'}}\n      </div>\n      <ion-list class="list-no-border address-block">\n        <ion-item>\n          <div item-left text-center>\n            <span class="round"></span>\n          </div>\n          <div>{{ trip.origin.vicinity }}</div>\n        </ion-item>\n        <ion-item>\n          <div item-left text-center>\n            <ion-icon name="pin" color="danger"></ion-icon>\n          </div>\n          <div>{{ trip.destination.vicinity }}</div>\n        </ion-item>\n      </ion-list>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\trips\trips.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_trip_service__["a" /* TripService */]])
    ], TripsPage);
    return TripsPage;
}());

//# sourceMappingURL=trips.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_driver_service__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var TripDetailPage = (function () {
    function TripDetailPage(nav, navParams, driverService) {
        var _this = this;
        this.nav = nav;
        this.navParams = navParams;
        this.driverService = driverService;
        this.trip = navParams.get('trip');
        this.driverService.getDriver(this.trip.driverId).take(1).subscribe(function (snapshot) {
            _this.driver = snapshot;
        });
    }
    // make array with range is n
    TripDetailPage.prototype.range = function (n) {
        return new Array(Math.round(n));
    };
    TripDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-detail',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\trip-detail\trip-detail.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ trip.pickedUpAt | amDateFormat: \'YYYY-MM-DD HH:mm\'}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="dark-bg" padding>\n    <span ion-text color="light" class="pull-left">Booking ID:</span>\n    <span ion-text color="light" class="pull-right">{{ trip.$key }}</span>\n    <div class="clear"></div>\n  </div>\n\n  <ion-list no-lines>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="{{ (driver)?.photoURL }}" alt="">\n      </ion-avatar>\n      <div>\n        {{ (driver)?.name }}\n      </div>\n    </ion-item>\n    <ion-item class="bg-color1">\n      Fare: <strong>{{ trip.currency }}{{ trip.fee }}</strong>\n    </ion-item>\n    <ion-item class="bg-color2">\n      <span ion-text color="dark">Notes to driver</span>\n      <br>\n      <span>{{ trip.note }}</span>\n    </ion-item>\n  </ion-list>\n\n  <ion-list class="list-no-border address-block">\n    <ion-item>\n      <div item-left text-center>\n        <span class="round"></span>\n      </div>\n      <div>{{ trip.origin.vicinity }}</div>\n    </ion-item>\n    <ion-item>\n      <div item-left text-center>\n        <ion-icon name="pin" color="danger"></ion-icon>\n      </div>\n      <div>{{ trip.destination.vicinity }}</div>\n    </ion-item>\n  </ion-list>\n\n  <div text-center>\n    <span>You rated</span>\n    <div class="stars" *ngIf="driver">\n      <ion-icon name="md-star" color="yellow" *ngFor="let star of range(trip.rating)"></ion-icon>\n      <ion-icon name="ios-star-outline" color="gray" *ngFor="let star of range(4.9 - trip.rating)"></ion-icon>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\trip-detail\trip-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_driver_service__["a" /* DriverService */]])
    ], TripDetailPage);
    return TripDetailPage;
}());

//# sourceMappingURL=trip-detail.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(858);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var UserPage = (function () {
    function UserPage(nav, authService, navParams, toastCtrl, loadingCtrl) {
        var _this = this;
        this.nav = nav;
        this.authService = authService;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.user = {
            photoURL: ''
        };
        var user = navParams.get('user');
        this.authService.getUser(user.uid).take(1).subscribe(function (snapshot) {
            snapshot.uid = snapshot.$key;
            _this.user = snapshot;
        });
    }
    // save user info
    UserPage.prototype.save = function () {
        this.authService.updateUserProfile(this.user);
        this.nav.pop();
        var toast = this.toastCtrl.create({
            message: 'Your profile has been updated',
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    // choose file for upload
    UserPage.prototype.chooseFile = function () {
        document.getElementById('avatar').click();
    };
    // upload thumb for item
    UserPage.prototype.upload = function () {
        var _this = this;
        // Create a root reference
        var storageRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        for (var _i = 0, _a = [document.getElementById('avatar').files[0]]; _i < _a.length; _i++) {
            var selectedFile = _a[_i];
            var path = '/users/' + Date.now() + ("" + selectedFile.name);
            var iRef = storageRef.child(path);
            iRef.put(selectedFile).then(function (snapshot) {
                loading.dismiss();
                _this.user.photoURL = snapshot.downloadURL;
            });
        }
    };
    UserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-user',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\user\user.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>User</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <!-- Cover and profile picture -->\n  <div class="driver-top primary-bg" text-center padding>\n    <div class="driver-picture">\n      <img class="profile-picture circle" src="{{ user.photoURL }}" (click)="chooseFile()">\n      <form ngNoForm>\n        <input id="avatar" name="file" type="file" (change)="upload()">\n      </form>\n    </div>\n    <h2 ion-text color="light">{{ user.name }}</h2>\n  </div>\n\n  <!-- User information -->\n  <ion-list class="list-full-border">\n\n    <ion-item>\n      <ion-label stacked color="primary">Name</ion-label>\n      <ion-input type="text" [(ngModel)]="user.name"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked color="primary">Phone</ion-label>\n      <ion-input type="text" [(ngModel)]="user.phoneNumber"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked color="primary">Email</ion-label>\n      <ion-input type="text" [(ngModel)]="user.email" disabled></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <div padding>\n    <button ion-button block color="primary" (click)="save()">Save</button>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\user\user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], UserPage);
    return UserPage;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 525:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(530);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 530:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__(567);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2__ = __webpack_require__(861);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angularfire2_database__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular2_moment__ = __webpack_require__(862);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_driver_service__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_notification_service__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_place_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_trip_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_setting_service__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_deal_service__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_auth_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_driver_driver__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_finding_finding__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_login_login__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_modal_rating_modal_rating__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_notification_notification__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_payment_method_payment_method__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_places_places__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_profile_profile__ = __webpack_require__(866);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_register_register__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_support_support__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_tracking_tracking__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_map_map__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_trips_trips__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_trip_detail_trip_detail__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_user_user__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_card_setting_card_setting__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// Import the AF2 Module



// Import moment module

// import services







// end import services
// import pages

















// end import pages
// AF2 Settings
var firebaseConfig = {
    apiKey: "AIzaSyD21WUv8fGMXo3Spf-Do2ch5KsxR0jJTJ4",
    authDomain: "appbrizzi.firebaseapp.com",
    databaseURL: "https://appbrizzi.firebaseio.com",
    projectId: "appbrizzi",
    storageBucket: "appbrizzi.appspot.com",
    messagingSenderId: "403197807591"
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__pages_driver_driver__["a" /* DriverPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_finding_finding__["a" /* FindingPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_modal_rating_modal_rating__["a" /* ModalRatingPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payment_method_payment_method__["a" /* PaymentMethodPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_places_places__["a" /* PlacesPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_support_support__["a" /* SupportPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_tracking_tracking__["a" /* TrackingPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_trips_trips__["a" /* TripsPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_trip_detail_trip_detail__["a" /* TripDetailPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_user_user__["a" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_card_setting_card_setting__["a" /* CardSettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_10_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_12_angular2_moment__["MomentModule"],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__pages_driver_driver__["a" /* DriverPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_finding_finding__["a" /* FindingPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_modal_rating_modal_rating__["a" /* ModalRatingPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_payment_method_payment_method__["a" /* PaymentMethodPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_places_places__["a" /* PlacesPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_support_support__["a" /* SupportPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_tracking_tracking__["a" /* TrackingPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_trips_trips__["a" /* TripsPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_trip_detail_trip_detail__["a" /* TripDetailPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_user_user__["a" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_card_setting_card_setting__["a" /* CardSettingPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_13__services_driver_service__["a" /* DriverService */],
                __WEBPACK_IMPORTED_MODULE_14__services_notification_service__["a" /* NotificationService */],
                __WEBPACK_IMPORTED_MODULE_15__services_place_service__["a" /* PlaceService */],
                __WEBPACK_IMPORTED_MODULE_16__services_trip_service__["a" /* TripService */],
                __WEBPACK_IMPORTED_MODULE_17__services_setting_service__["a" /* SettingService */],
                __WEBPACK_IMPORTED_MODULE_18__services_deal_service__["a" /* DealService */],
                __WEBPACK_IMPORTED_MODULE_19__services_auth_service__["a" /* AuthService */],
                /* import services */
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 567:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_notification_notification__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_support_support__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_trips_trips__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth_auth__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_auth_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_user_user__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_card_setting_card_setting__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import pages









// end import pages
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, afAuth, authService) {
        var _this = this;
        this.afAuth = afAuth;
        this.authService = authService;
        this.user = {};
        this.pages = [
            {
                title: 'Home',
                icon: 'ios-home-outline',
                count: 0,
                component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]
            },
            {
                title: 'History',
                icon: 'ios-time-outline',
                count: 0,
                component: __WEBPACK_IMPORTED_MODULE_8__pages_trips_trips__["a" /* TripsPage */]
            },
            {
                title: 'Card setting',
                icon: 'ios-card-outline',
                count: 0,
                component: __WEBPACK_IMPORTED_MODULE_12__pages_card_setting_card_setting__["a" /* CardSettingPage */]
            },
            {
                title: 'Notification',
                icon: 'ios-notifications-outline',
                count: 2,
                component: __WEBPACK_IMPORTED_MODULE_6__pages_notification_notification__["a" /* NotificationPage */]
            },
            {
                title: 'Support',
                icon: 'ios-help-circle-outline',
                count: 0,
                component: __WEBPACK_IMPORTED_MODULE_7__pages_support_support__["a" /* SupportPage */]
            },
        ];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            // check for login stage, then redirect
            afAuth.authState.take(1).subscribe(function (authData) {
                if (authData) {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]);
                }
                else {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
                }
            });
            // get user data
            afAuth.authState.subscribe(function (authData) {
                if (authData) {
                    _this.user = authService.getUserData();
                }
            });
        });
    }
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    // view current user profile
    MyApp.prototype.viewProfile = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__pages_user_user__["a" /* UserPage */], {
            user: this.user
        });
    };
    // logout
    MyApp.prototype.logout = function () {
        var _this = this;
        this.authService.logout().then(function () {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-content class="menu-left">\n    <!-- User profile -->\n    <div text-center padding-top padding-bottom class="primary-bg menu-left">\n      <a (click)="viewProfile()" menuClose>\n        <img class="profile-picture" src="{{ user.photoURL }}">\n        <h4 ion-text color="light">{{ user.displayName }}</h4>\n      </a>\n    </div>\n\n    <ion-list class="list-full-border">\n      <button ion-item menuClose *ngFor="let page of pages" (click)="openPage(page)">\n        <ion-icon item-left name="{{ page.icon }}"></ion-icon>\n        {{ page.title }}\n        <ion-badge color="danger" item-right *ngIf="page.count">{{ page.count }}</ion-badge>\n      </button>\n      <button ion-item menuClose (click)="logout()">\n        <ion-icon item-left name="ios-exit-outline"></ion-icon>\n        Logout\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\app\app.html"*/,
            queries: {
                nav: new __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"]('content')
            }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_10__services_auth_service__["a" /* AuthService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Place; });
var Place = (function () {
    function Place(vicinity, lat, lng) {
        this.lat = lat;
        this.lng = lng;
        this.vicinity = vicinity;
    }
    // get place object with formatted data
    Place.prototype.getFormatted = function () {
        return {
            location: {
                lat: this.lat,
                lng: this.lng
            },
            vicinity: this.vicinity
        };
    };
    return Place;
}());

//# sourceMappingURL=place.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PlaceService = (function () {
    function PlaceService(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
    PlaceService.prototype.calcCrow = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // km
        var dLat = this.toRad(lat2 - lat1);
        var dLon = this.toRad(lon2 - lon1);
        lat1 = this.toRad(lat1);
        lat2 = this.toRad(lat2);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    // Converts numeric degrees to radians
    PlaceService.prototype.toRad = function (value) {
        return value * Math.PI / 180;
    };
    /**
     * Convert geocoder address to place object
     * @param address: Geocoder address result
     * @returns {{location: {lat: any, lng: any}, vicinity: string}}
     */
    PlaceService.prototype.formatAddress = function (address) {
        console.log(address);
        var components = address.address_components;
        var vicinity = components[0].short_name + ', ' + components[1].short_name;
        return {
            location: {
                lat: address.geometry.location.lat(),
                lng: address.geometry.location.lng()
            },
            vicinity: vicinity
        };
    };
    // set locality from geocoder result
    // @param results: Geocoder array results
    PlaceService.prototype.setLocalityFromGeocoder = function (results) {
        var component;
        var address;
        for (var i = 0; i < results.length; i++) {
            address = results[i];
            for (var j = 0; j < address.address_components.length; j++) {
                component = address.address_components[j];
                if (component.types[0] == 'administrative_area_level_1') {
                    // if (component.types[0] == 'locality') {
                    // escape firebase characters
                    var locality = component.short_name.replace(/[\%\.\#\$\/\[\]]/, '_');
                    this.setLocality(locality);
                    return locality;
                }
            }
        }
        return false;
    };
    PlaceService.prototype.setLocality = function (locality) {
        return this.locality = locality;
    };
    PlaceService.prototype.getLocality = function () {
        return this.locality;
    };
    PlaceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], PlaceService);
    return PlaceService;
}());

//# sourceMappingURL=place-service.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return SHOW_VEHICLES_WITHIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return POSITION_INTERVAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return VEHICLE_LAST_ACTIVE_LIMIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DEAL_STATUS_PENDING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DEAL_STATUS_ACCEPTED; });
/* unused harmony export DEAL_TIMEOUT */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return TRIP_STATUS_GOING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return TRIP_STATUS_FINISHED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return DEFAULT_AVATAR; });
var SHOW_VEHICLES_WITHIN = 5; // within 5km
var POSITION_INTERVAL = 10000; // 2000ms
var VEHICLE_LAST_ACTIVE_LIMIT = 30000; // 30s
var DEAL_STATUS_PENDING = 'pending';
var DEAL_STATUS_ACCEPTED = 'accepted';
var DEAL_TIMEOUT = 20000; // 20s
var TRIP_STATUS_GOING = 'going';
var TRIP_STATUS_FINISHED = 'finished';
var DEFAULT_AVATAR = 'https://freeiconshop.com/wp-content/uploads/edd/person-outline-filled.png';
/*
 trip status: waiting, going, finished

 */ 
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DriverService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DriverService = (function () {
    function DriverService(db) {
        this.db = db;
    }
    // get driver by id
    DriverService.prototype.getDriver = function (id) {
        return this.db.object('drivers/' + id);
    };
    // get driver position
    DriverService.prototype.getDriverPosition = function (locality, vehicleType, id) {
        return this.db.object('localities/' + locality + '/' + vehicleType + '/' + id);
    };
    DriverService.prototype.getActiveDriver = function (locality, vehicleType) {
        return this.db.list('localities/' + locality + '/' + vehicleType);
    };
    // calculate vehicle angle
    DriverService.prototype.calcAngle = function (oldLat, oldLng, lat, lng) {
        var brng = Math.atan2(lat - oldLat, lng - oldLng);
        brng = brng * (180 / Math.PI);
        return brng;
    };
    // return icon suffix by angle
    DriverService.prototype.getIconWithAngle = function (vehicle) {
        var angle = this.calcAngle(vehicle.oldLat, vehicle.oldLng, vehicle.lat, vehicle.lng);
        if (angle >= -180 && angle <= -160) {
            return '_left';
        }
        if (angle > -160 && angle <= -110) {
            return '_bottom_left';
        }
        if (angle > -110 && angle <= -70) {
            return '_bottom';
        }
        if (angle > -70 && angle <= -20) {
            return '_bottom_right';
        }
        if (angle >= -20 && angle <= 20) {
            return '_right';
        }
        if (angle > 20 && angle <= 70) {
            return '_top_right';
        }
        if (angle > 70 && angle <= 110) {
            return '_top';
        }
        if (angle > 110 && angle <= 160) {
            return '_top_left';
        }
        if (angle > 160 && angle <= 180) {
            return '_left';
        }
    };
    DriverService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], DriverService);
    return DriverService;
}());

//# sourceMappingURL=driver-service.js.map

/***/ }),

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NOTIFICATIONS; });
var NOTIFICATIONS = [
    {
        id: 1,
        title: "New price from Jan 2016",
        content: "",
        createdAt: "2016-02-14 12:00:00",
        read: true
    },
    {
        id: 2,
        title: "New version 1.1.1",
        content: "",
        createdAt: "2016-02-13 12:00:00",
        read: false
    },
    {
        id: 3,
        title: "New version 1.1.0",
        content: "",
        createdAt: "2016-02-12 12:00:00",
        read: false
    }
];
//# sourceMappingURL=mock-notifications.js.map

/***/ }),

/***/ 864:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 390,
	"./af.js": 390,
	"./ar": 391,
	"./ar-dz": 392,
	"./ar-dz.js": 392,
	"./ar-kw": 393,
	"./ar-kw.js": 393,
	"./ar-ly": 394,
	"./ar-ly.js": 394,
	"./ar-ma": 395,
	"./ar-ma.js": 395,
	"./ar-sa": 396,
	"./ar-sa.js": 396,
	"./ar-tn": 397,
	"./ar-tn.js": 397,
	"./ar.js": 391,
	"./az": 398,
	"./az.js": 398,
	"./be": 399,
	"./be.js": 399,
	"./bg": 400,
	"./bg.js": 400,
	"./bm": 401,
	"./bm.js": 401,
	"./bn": 402,
	"./bn.js": 402,
	"./bo": 403,
	"./bo.js": 403,
	"./br": 404,
	"./br.js": 404,
	"./bs": 405,
	"./bs.js": 405,
	"./ca": 406,
	"./ca.js": 406,
	"./cs": 407,
	"./cs.js": 407,
	"./cv": 408,
	"./cv.js": 408,
	"./cy": 409,
	"./cy.js": 409,
	"./da": 410,
	"./da.js": 410,
	"./de": 411,
	"./de-at": 412,
	"./de-at.js": 412,
	"./de-ch": 413,
	"./de-ch.js": 413,
	"./de.js": 411,
	"./dv": 414,
	"./dv.js": 414,
	"./el": 415,
	"./el.js": 415,
	"./en-au": 416,
	"./en-au.js": 416,
	"./en-ca": 417,
	"./en-ca.js": 417,
	"./en-gb": 418,
	"./en-gb.js": 418,
	"./en-ie": 419,
	"./en-ie.js": 419,
	"./en-il": 420,
	"./en-il.js": 420,
	"./en-nz": 421,
	"./en-nz.js": 421,
	"./eo": 422,
	"./eo.js": 422,
	"./es": 423,
	"./es-do": 424,
	"./es-do.js": 424,
	"./es-us": 425,
	"./es-us.js": 425,
	"./es.js": 423,
	"./et": 426,
	"./et.js": 426,
	"./eu": 427,
	"./eu.js": 427,
	"./fa": 428,
	"./fa.js": 428,
	"./fi": 429,
	"./fi.js": 429,
	"./fo": 430,
	"./fo.js": 430,
	"./fr": 431,
	"./fr-ca": 432,
	"./fr-ca.js": 432,
	"./fr-ch": 433,
	"./fr-ch.js": 433,
	"./fr.js": 431,
	"./fy": 434,
	"./fy.js": 434,
	"./gd": 435,
	"./gd.js": 435,
	"./gl": 436,
	"./gl.js": 436,
	"./gom-latn": 437,
	"./gom-latn.js": 437,
	"./gu": 438,
	"./gu.js": 438,
	"./he": 439,
	"./he.js": 439,
	"./hi": 440,
	"./hi.js": 440,
	"./hr": 441,
	"./hr.js": 441,
	"./hu": 442,
	"./hu.js": 442,
	"./hy-am": 443,
	"./hy-am.js": 443,
	"./id": 444,
	"./id.js": 444,
	"./is": 445,
	"./is.js": 445,
	"./it": 446,
	"./it.js": 446,
	"./ja": 447,
	"./ja.js": 447,
	"./jv": 448,
	"./jv.js": 448,
	"./ka": 449,
	"./ka.js": 449,
	"./kk": 450,
	"./kk.js": 450,
	"./km": 451,
	"./km.js": 451,
	"./kn": 452,
	"./kn.js": 452,
	"./ko": 453,
	"./ko.js": 453,
	"./ky": 454,
	"./ky.js": 454,
	"./lb": 455,
	"./lb.js": 455,
	"./lo": 456,
	"./lo.js": 456,
	"./lt": 457,
	"./lt.js": 457,
	"./lv": 458,
	"./lv.js": 458,
	"./me": 459,
	"./me.js": 459,
	"./mi": 460,
	"./mi.js": 460,
	"./mk": 461,
	"./mk.js": 461,
	"./ml": 462,
	"./ml.js": 462,
	"./mn": 463,
	"./mn.js": 463,
	"./mr": 464,
	"./mr.js": 464,
	"./ms": 465,
	"./ms-my": 466,
	"./ms-my.js": 466,
	"./ms.js": 465,
	"./mt": 467,
	"./mt.js": 467,
	"./my": 468,
	"./my.js": 468,
	"./nb": 469,
	"./nb.js": 469,
	"./ne": 470,
	"./ne.js": 470,
	"./nl": 471,
	"./nl-be": 472,
	"./nl-be.js": 472,
	"./nl.js": 471,
	"./nn": 473,
	"./nn.js": 473,
	"./pa-in": 474,
	"./pa-in.js": 474,
	"./pl": 475,
	"./pl.js": 475,
	"./pt": 476,
	"./pt-br": 477,
	"./pt-br.js": 477,
	"./pt.js": 476,
	"./ro": 478,
	"./ro.js": 478,
	"./ru": 479,
	"./ru.js": 479,
	"./sd": 480,
	"./sd.js": 480,
	"./se": 481,
	"./se.js": 481,
	"./si": 482,
	"./si.js": 482,
	"./sk": 483,
	"./sk.js": 483,
	"./sl": 484,
	"./sl.js": 484,
	"./sq": 485,
	"./sq.js": 485,
	"./sr": 486,
	"./sr-cyrl": 487,
	"./sr-cyrl.js": 487,
	"./sr.js": 486,
	"./ss": 488,
	"./ss.js": 488,
	"./sv": 489,
	"./sv.js": 489,
	"./sw": 490,
	"./sw.js": 490,
	"./ta": 491,
	"./ta.js": 491,
	"./te": 492,
	"./te.js": 492,
	"./tet": 493,
	"./tet.js": 493,
	"./tg": 494,
	"./tg.js": 494,
	"./th": 495,
	"./th.js": 495,
	"./tl-ph": 496,
	"./tl-ph.js": 496,
	"./tlh": 497,
	"./tlh.js": 497,
	"./tr": 498,
	"./tr.js": 498,
	"./tzl": 499,
	"./tzl.js": 499,
	"./tzm": 500,
	"./tzm-latn": 501,
	"./tzm-latn.js": 501,
	"./tzm.js": 500,
	"./ug-cn": 502,
	"./ug-cn.js": 502,
	"./uk": 503,
	"./uk.js": 503,
	"./ur": 504,
	"./ur.js": 504,
	"./uz": 505,
	"./uz-latn": 506,
	"./uz-latn.js": 506,
	"./uz.js": 505,
	"./vi": 507,
	"./vi.js": 507,
	"./x-pseudo": 508,
	"./x-pseudo.js": 508,
	"./yo": 509,
	"./yo.js": 509,
	"./zh-cn": 510,
	"./zh-cn.js": 510,
	"./zh-hk": 511,
	"./zh-hk.js": 511,
	"./zh-tw": 512,
	"./zh-tw.js": 512
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 864;

/***/ }),

/***/ 866:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
 Generated class for the ProfilePage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(nav) {
        this.nav = nav;
    }
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\profile\profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Profile</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="profile">\n\n</ion-content>\n'/*ion-inline-end:"D:\successdt-ion_firebase_taxi-1bc8d003a4a3\passenger\src\pages\profile\profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

},[525]);
//# sourceMappingURL=main.js.map